import { Module } from '@nestjs/common';
import { ASYNC_STORAGE, RequestContext } from './index';
import { AsyncLocalStorage } from 'async_hooks';

@Module({
  providers: [
    {
      provide: ASYNC_STORAGE,
      useValue: new AsyncLocalStorage<RequestContext>(),
    },
  ],
  exports: [ASYNC_STORAGE],
})
export class RequestContextModule {}
