export const ASYNC_STORAGE = 'ASYNC_STORAGE';
export const CORRELATION_ID = 'correlationId';
export const CORRELATION_ID_HEADER = 'x-correlation-id';

export interface RequestContext {
  readonly correlationId: string;
}
